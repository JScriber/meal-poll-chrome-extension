const usernameInput = document.getElementById('username') as HTMLInputElement;

// Set the value at startup.
chrome.storage.sync.get(({ token }) => usernameInput.value = token || '');

// Listen for form submition.
document.getElementById('optionForm').addEventListener('submit', (e) => {
  e.preventDefault();

  const username = usernameInput.value;

  if (username.trim().length > 0) {
    chrome.storage.sync.set({ 'token': username }, () => window.close());
  }
});

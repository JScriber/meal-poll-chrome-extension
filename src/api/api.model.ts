export interface Group {
  id: number;
  name: string;
  picture: string;
  members: string[];
}

export interface Groups {

  /** Group the user has already selected. */
  selected: number | undefined;

  /** Existing groups. */
  groups: Group[];
}

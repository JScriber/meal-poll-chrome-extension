import { Groups } from './api.model';
import { environment } from '../environment';

export interface Api {

  /** Access token. */
  token: string;

  /** Fetches all the groups. */
  getGroups(): Promise<Groups>;

  /**
   * Joins the group with the given ID.
   * @param {number} id
   */
  joinGroup(id: number): Promise<void>;

}

/**
 * Http implementation of the {@link Api} interface.
 */
export class HttpApi implements Api {

  /** Access token. */
  token: string;

  /** @inheritdoc */
  getGroups(): Promise<Groups> {

    // TODO: Replace with real call.

    return new Promise((resolve) => resolve({
      selected: 2,
      groups: [
        {
          id: 0,
          name: 'E.Leclerc',
          picture: './images/places/leclerc.jpg',
          members: [
            'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50',
            'https://www.gravatar.com/avatar/00000000000000000000000000000000'
          ]
        },
        {
          id: 1,
          name: 'McDonald',
          picture: './images/places/mcdonald.jpg',
          members: [
             'https://www.gravatar.com/avatar/00000000000000000000000000000000'
          ]
        },
        {
          id: 2,
          name: 'Traiteur Guillemois',
          picture: './images/places/guillemois.jpg',
          members: [
            'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50'
          ]
        },
        {
          id: 8,
          name: 'J\'ai ma nourriture',
          picture: './images/places/enterprise.jpg',
          members: [
            'https://www.gravatar.com/avatar/00000000000000000000000000000000'
          ]
        },
        {
          id: 3,
          name: 'A la part',
          picture: './images/places/a-la-part.jpg',
          members: []
        },
        {
          id: 4,
          name: 'Au comptoir des pizzas',
          picture: './images/places/comptoir-des-pizzas.jpg',
          members: []
        },
        {
          id: 5,
          name: 'Boucherie les 2M',
          picture: './images/places/les-2m.jpg',
          members: []
        },
        {
          id: 6,
          name: 'Marché - Galette',
          picture: './images/places/galette.jpg',
          members: []
        },
        {
          id: 7,
          name: 'La Farinette',
          picture: './images/places/la-farinette.jpg',
          members: []
        }
      ]
    }));
  }

  /** @inheritdoc */
  async joinGroup(id: number): Promise<void> {

    await fetch(`${environment.apiUrl}/group/${id}`, {
      method: 'PUT'
    });
  }
}

/** Enables foldables. */
export const initFoldables = () => {

  document.querySelectorAll('.foldable-header').forEach(f => f.addEventListener('click', function () {
    this.parentNode.classList.toggle('fold');
  }))
};

export const applyClassOnBody = (state: boolean , className: string) => {
  const body = document.querySelector('body');

  state ? body.classList.add(className) : body.classList.remove(className);
};

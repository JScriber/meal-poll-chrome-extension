import { Renderer } from './renderer';
import { HttpApi, Api } from '../api/api';
import { initFoldables } from './utils';

/**
 * Main application class.
 */
class Application {

  /** Unique instance of the application. */
  private static _instance: Application;

  /** Always return the same instance of the application. */
  static get instance() {
    if (!Application._instance) Application._instance = new this();

    return Application._instance;
  }

  /** Use the HTTP implementation of the API call. */
  private readonly api: Api = new HttpApi();

  /** Class that renders the data. */
  private readonly renderer = new Renderer();

  private constructor() {

    // Fetch user informations.
    chrome.storage.sync.get(({ token }) => {
      if (!token) {
        chrome.runtime.openOptionsPage();
        window.close();
      }

      this.api.token = token;
    });

    initFoldables();
    this.listenSubmit();

    // Load the reference to the templates for the first time.
    Renderer.loadTemplates();
  }

  /** Fetch and render the data. */
  async fetchData() {
    try {
      const { selected, groups } = await this.api.getGroups();
  
      // Show the groups.
      this.renderer.renderGroups(selected, groups.filter(g => g.members.length > 0));
  
      // Show the available places.
      this.renderer.renderAvailables(groups.filter(g => g.members.length === 0));
  
      this.renderer.setLoadingState(false);
      this.renderer.setServerState(true);
    } catch(e) {
      this.serverNotFound();
    }
  }

  /** Listens for form submition. */
  private listenSubmit() {
  
    document.getElementById('form').addEventListener('submit', async (e) => {
      e.preventDefault();
  
      const input = document.querySelector('input[name="group"]:checked') as HTMLInputElement;
  
      if (input) {
        const ID = +input.value;
  
        this.renderer.setLoadingState(true);

        try {
          await this.api.joinGroup(ID);
          await this.fetchData();
        } catch(e) {
          this.renderer.setLoadingState(false);
          this.serverNotFound();
        }
      }
    });
  }

  /** Method called whenever the server is not found. */
  private serverNotFound() {
    console.error('404 - Server not found!');

    this.renderer.setServerState(false);
  }
}

/** Main entrypoint. */
document.addEventListener('DOMContentLoaded', async () => await Application.instance.fetchData(), false);

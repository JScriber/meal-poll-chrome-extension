import { Group } from '../api/api.model';
import { applyClassOnBody } from './utils';


/**
 * Renders data in the view.
 */
export class Renderer {

  /** Template for groups. */
  private static GROUP_TEMPLATE: HTMLElement;

  /** Template for available place to eat. */
  private static AVAILABLE_TEMPLATE: HTMLElement;

  /**
   * Loads the reference to the templates.
   */
  static loadTemplates() {
    // Set the group model reference.
    Renderer.GROUP_TEMPLATE = document.getElementById('group-template');

    // Set the availables model reference.
    Renderer.AVAILABLE_TEMPLATE = document.getElementById('available-template');
  }

  /**
   * Renders the given groups.
   * @param selected
   * @param groups 
   */
  renderGroups(selected: number | undefined, groups: Group[]) {
    const groupsDOM: HTMLElement = document.getElementById('groups');

    // Remove children.
    this.deleteChildren(groupsDOM);

    // Sort the groups.
    groups = groups.sort((a, b) => a.members.length > b.members.length ? -1 : 1);

    groups.forEach(group => {
      const template = this.duplicateTemplate(Renderer.GROUP_TEMPLATE);
      const input = template.querySelector('input');

      // Set the value and the unique ID.
      const ID = `group_${group.id}`;
      input.setAttribute('id', ID);
      input.value = group.id + '';

      input.checked = selected === group.id;

      template.querySelector('label').setAttribute('for', ID);

      // Set the name.
      template.querySelector('.name').innerHTML = group.name;
  
      // Set the picture of the group.
      const groupPicture: HTMLImageElement = template.querySelector('.place-to-eat img');
      groupPicture.setAttribute('src', group.picture);
  
      // Set the members.
      const eater = template.querySelector('.eater');

      group.members.forEach(picture => {
        const img = document.createElement('IMG') as HTMLImageElement;
        img.setAttribute('src', picture);
  
        eater.appendChild(img);
      });
  
      groupsDOM.appendChild(template);
    });
  }

  /**
   * Displays the availables.
   * @param {*} availables 
   */
  renderAvailables(availables: Group[]) {

    const availablesDOM = document.getElementById('availables');

    // Remove children.
    this.deleteChildren(availablesDOM);

    availables.forEach(available => {
      const template = this.duplicateTemplate(Renderer.AVAILABLE_TEMPLATE);
      const input = template.querySelector('input');

      // Set input values.
      const ID = `available_${available.id}`;

      input.setAttribute('id', ID);
      input.value = available.id + '';

      template.querySelector('label').setAttribute('for', ID);

      // Update the picture.
      template.querySelector('img').setAttribute('src', available.picture);

      // Set the name.
      template.querySelector('p.name').innerHTML = available.name;

      availablesDOM.appendChild(template);
    });
  }

  /** Change the loading state. */
  setLoadingState(state: boolean) {
    applyClassOnBody(state, 'loading');
  }

  /** Change the server-not-found state. */
  setServerState(state: boolean) {
    applyClassOnBody(!state, 'server-not-found');
  }

  /**
   * Deletes all the children of the parent.
   * @param {*} parent 
   */
  private deleteChildren(parent: HTMLElement) {
    while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
    }
  };

  /**
   * Generates a group template.
   * @param {*} model
   * @returns {HTMLElement}
   */
  private duplicateTemplate(model: any): HTMLElement {
    const duplicate = model.cloneNode(true);

    duplicate.removeAttribute('id');

    return duplicate;
  }
}
